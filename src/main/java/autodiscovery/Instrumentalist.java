package autodiscovery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("eddie")
public class Instrumentalist implements Performer {

	@Autowired
	private Instrument instrument;
	
	public Instrumentalist() {
	}

	@Override
	public void perform() {
		// TODO Auto-generated method stub
		this.instrument.play();
	}

	/**
	 * @return the instrument
	 */
	public Instrument getInstrument() {
		return instrument;
	}

	/**
	 * @param instrument the instrument to set
	 */
	public void setInstrument(Instrument instrument) {
		this.instrument = instrument;
	}

}
