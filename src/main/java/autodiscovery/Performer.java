package autodiscovery;

public interface Performer {
	public void perform();
}
