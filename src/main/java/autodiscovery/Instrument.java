package autodiscovery;

public interface Instrument {
	public void play();
}
