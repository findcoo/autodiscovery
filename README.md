##Autodiscovery

1. *디렉토리 구성*
```
./src/
└── main
    ├── java
    │   └── autodiscovery
    │       ├── Guitar.java
    │       ├── Instrumentalist.java
    │       ├── Instrument.java
    │       ├── Main.java
    │       └── Performer.java
    └── resources
        └── autodiscovery.xml

```
---
2. *주의 및 참고*
	+ xml 설정에 `<context:component-scan>` 태그를 통해 자동으로 빈을 찾도록 설정한다.
	+ 빈으로 구현될 모든 클래스위에 `@Component` 어노테이션을 쓴다.
	+ 빈 어노테이션의 종류
		+ `@Component` - 범용 스테레오 타입, 클래스가 스프링 컴포넌트를 알려준다.
		+ `@Controller` - 스프링 MVC 컨트롤러 임을 알려준다.
		+ `@Repository` - 클래스가 데이터 저장소임을 알려준다.
		+ `@Service` - 클래스가 서비스임을 알려준다.
	+ 스캔 필터링
		+ 컴포넌트를 번잡하게 선언하는 것을 피하기위해 스캔 필터링을 추가한다.
		+ `<context:include-filter type="aasignable" expression="CLASS PATH DNS">` 의 형태로 선언하며 해당 클래스에 필요한 모든 빈은 자동 컴포넌트로 등록된다.
		
---
3. *기타*
	1. 스캔 필터링 타입
---
| 필터 타입 | 설명 |
|:--------|:----:|
| annotation | 클래스에 지정한 애너테이션이 적용됐는지의 여부. |
| assignable | 클래스가 지정한 타입으로 할당 가능한지의 여부. |
| aspectj | 클래스가 Aspectj 표현식에 일치하는지 여부. |
| custom | TypeFilter의 구현체를 사용. |
| regex | 클래스 이름이 정규 표현식에 일치하는지의 여부. |
---
